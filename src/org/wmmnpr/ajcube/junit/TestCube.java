/*
* Copyright (C) 2015  wmmnpr@gmail.com
*
*This program is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*This program is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
package org.wmmnpr.ajcube.junit;

import static org.junit.Assert.*;

import java.io.FileInputStream;

import org.junit.Test;
import org.junit.Before;
import org.wmmnpr.ajcube.*;

public class TestCube {
	
	
	public Cube cube;
	
	@Before
	public void setUp(){
		this.cube = new Cube();
	}

    //@Test 
	public void testSaveCube() throws Exception{
	
		this.cube = new Cube();
		
		this.cube.rotate(Side.RED, Direction.INVERTED);
		
		cube.save(System.out);
	}
	
    //@Test
    public void loadFromXml() throws Exception {
    	
    	final String inputName = "etc\\cube02.xml";
    	FileInputStream ins = new FileInputStream(inputName);
    	Cube lcube = Cube.load(ins);
    	lcube.printAsMap(System.out);
    	
    	this.cube = new Cube();
    	this.cube.rotate(Side.RED, Direction.INVERTED);
    	
    	assertTrue(this.cube.equals(lcube));
    	
    }
	
	//@Test
	public void test() {

		System.out.println(String.format("TestCube.- cube is %s", this.cube));
		this.cube.rotate(Side.GREEN, Direction.INVERTED);		
		System.out.println("\n");		
		this.cube.printAsMap(System.out);
	}

	
	//@Test
	public void testAllSides(){
		
		Cube refCube = new Cube();	
		Side sides[] = {Side.RED, Side.YELLOW, Side.WHITE, Side.ORANGE, Side.BLUE, Side.GREEN};

		Cube testCube = null;
		for(Side side : sides){
			
			testCube = new Cube();
			System.out.println(String.format("\n\n---------------Testing %s %s -----------------", side, Direction.NORMAL));
			
			testCube.rotate(side, Direction.NORMAL);		
			testCube.printAsMap(System.out);
			
			testCube = new Cube();
			System.out.println(String.format("\n\n---------------Testing %s %s -----------------", side, Direction.INVERTED));
			
			testCube.rotate(side, Direction.INVERTED);		
			testCube.printAsMap(System.out);
				
		}
			
	}
	
	//@Test
	public void test2ndLayerSwap() {

		this.cube = new Cube();

		System.out.println("\n\nTesting 2nd Layer Swap : intial");
	
		this.cube.rotate(Side.WHITE, Direction.NORMAL);			
		this.cube.rotate(Side.GREEN, Direction.NORMAL);	
		this.cube.rotate(Side.WHITE, Direction.INVERTED);	
		this.cube.rotate(Side.GREEN, Direction.INVERTED);	
		this.cube.rotate(Side.WHITE, Direction.INVERTED);	
		this.cube.rotate(Side.RED, Direction.INVERTED);	
		this.cube.rotate(Side.WHITE, Direction.NORMAL);	
		this.cube.rotate(Side.RED, Direction.NORMAL);	
		this.cube.printAsMap(System.out);
		
		//compare with original
		//assertTrue(cube.equals(start));
		
	}
	//@Test
	public void testYellow() {

		Cube start = new Cube(cube);

		System.out.println("\n\nTesting Yellow : intial");
		this.cube.printAsMap(System.out);
		
		//clockwise
		System.out.println("\n\nYellow ");		
		this.cube.rotate(Side.YELLOW, Direction.NORMAL);			
		this.cube.printAsMap(System.out);
		
		//counter clockwise
		System.out.println("\n\nYellow inverted");		
		this.cube.rotate(Side.YELLOW, Direction.INVERTED);
		this.cube.printAsMap(System.out);
		
		//compare with original
		assertTrue(cube.equals(start));
		
	}	
	//@Test
	public void testGreen() {

		Cube start = new Cube(cube);

		System.out.println("\n\nTesting Green : intial");
		this.cube.printAsMap(System.out);
		
		//clockwise
		System.out.println("\n\nGreen ");		
		this.cube.rotate(Side.GREEN, Direction.NORMAL);			
		this.cube.printAsMap(System.out);
		
		//counter clockwise
		//System.out.println("\n\nGreen inverted");		
		//this.cube.rotate(Side.GREEN, Direction.NORMAL);
		//this.cube.printAsMap(System.out);
		
		//compare with original
		assertTrue(cube.equals(start));
		
	}
	
	//@Test
	public void testOrange() {

		Cube start = new Cube(cube);

		System.out.println("\n\nTesting Orange : intial");
		this.cube.printAsMap(System.out);
		
		//clockwise
		System.out.println("\n\nOrange ");		
		this.cube.rotate(Side.ORANGE, Direction.NORMAL);			
		this.cube.printAsMap(System.out);
		
		//counter clockwise
		System.out.println("\n\nOrange inverted");		
		this.cube.rotate(Side.ORANGE, Direction.INVERTED);
		this.cube.printAsMap(System.out);
		
		//compare with original
		assertTrue(cube.equals(start));
		
	}
	
	
	
    //@Test
	public void testCornerSwap() {

		System.out.println("\n\nTesting corner swap first layer.");
		Cube start = new Cube();		
		start.printAsMap(System.out);
		start.rotate(Side.GREEN, Direction.INVERTED);
		start.printAsMap(System.out);
		start.rotate(Side.ORANGE, Direction.INVERTED);
		start.printAsMap(System.out);
		start.rotate(Side.GREEN, Direction.NORMAL);	
		start.printAsMap(System.out);
		start.rotate(Side.ORANGE, Direction.INVERTED);		
		start.printAsMap(System.out);
			
	}
	
	//@Test
	public void testMap() {

		System.out.println("\n\nTesting printAsMap.");
		Cube start = new Cube();		
	
		start.printAsMap(System.out);
			
	}
		
	
	//@Test
	public void testRed() {
		Cube start = new Cube(cube);

		System.out.println("\n\nTesting Red : intial");
		this.cube.print(System.out);
		
		//clockwise
		System.out.println("\n\nRed ");		
		this.cube.rotate(Side.RED, Direction.NORMAL);	
		this.cube.printAsMap(System.out);
		
		//counter clockwise
		System.out.println("\n\nRed inverted");		
		this.cube.rotate(Side.RED, Direction.INVERTED);
		this.cube.printAsMap(System.out);
		
		//compare with original
		assertTrue(cube.equals(start));
	}	

	//@Test
	public void testRedGreen() {
		Cube start = new Cube(cube);

		System.out.println("\n\nTesting Red - Green : intial");
		this.cube.print(System.out);
		
		for(int i=0; i < 1000; i++){
			
			cube.rotate(Side.RED, Direction.NORMAL);
			cube.rotate(Side.GREEN, Direction.NORMAL);

			System.out.println("\n\nRed,Green");		
			this.cube.printAsMap(System.out);
			
			
			if(cube.equals(start)){
				System.out.println(String.format("\nCubes are equal again after %s moves.", i));
				break;
			}
			
		}
	
	}	
	
	/**
			// 2   11  20           20   23   26           
			// 5   14  23  --G'->   11   14   17
			// 8   17  26   
	 */
	//@Test
	public void testLoop(){
		for(int k=0; k < 3; k++){
			for(int j=0; j<3; j++){	
				for(int i=2; i==2; i++){						
					System.out.print(String.format("[%2s]:", ((9*k)+(3*j)+(1+i))-1));
				}
			}
		}			
	}
	
	//@Test
	public void testRandomize() throws Exception{
		
		System.out.println(this.cube.randomize(6, System.out));
	}
	
	@Test
	public void testCheck(){
		
		Cube cube1 = new Cube();
		Cube cube2 = new Cube();
						
		//rotate just top/green corners. All edges stay the same.
		cube1.runAlgorithm("G,W,g,y,G,w,g,Y");
				
		cube1.printAsMap(System.out);
		
		//cubes should not be equal
		System.out.println(String.format("cube2 == cube1 is %s", cube2.equals(cube1))); 
		
		int edges[] = {1, 3, 5, 7, 9, 11, 15, 17, 19, 21, 23, 25};
		Cubelet cubelets1[] = new Cubelet[12];	
		int idx = 0;
		for(int edge : edges){
			cubelets1[idx++] = cube1.getCubelets()[edge];
		}

		//but there edges should be
		System.out.println(String.format("edges of cube2 == edges cube1 is %s", cube2.check(cubelets1))); 

		
	}
}
