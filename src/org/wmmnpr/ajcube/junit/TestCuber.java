/*
* Copyright (C) 2015  wmmnpr@gmail.com
*
*This program is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*This program is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
package org.wmmnpr.ajcube.junit;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.junit.Test;
import org.wmmnpr.ajcube.Cube;
import org.wmmnpr.ajcube.Cuber;
import org.wmmnpr.ajcube.solver.DepthFirstSearch;

public class TestCuber {

	//@Test
	public void test() throws Exception {	
		Cube cube = Cube.load(new FileInputStream("etc\\cube06.xml"));
		cube.printAsMap(System.out);
		Cuber cuber = new Cuber();
	
		cuber.setCube(cube);
		//"r,g,R,w"  (checked 2015.08.26)
		//cuber.runAlgorithm("r,g,R,w");
		//cuber.runAlgorithm("W,r,G,R");
		
		//"R,G,r,Y" (checked 2015.08.26)
		//cuber.runAlgorithm("R,G,r,Y");
		//cuber.runAlgorithm("y,R,g,r");
		
		//"Y,o,G,O" (checked 2015.08.26)
		//cuber.runAlgorithm("Y,o,G,O");			
		//cuber.runAlgorithm("o,g,O,y");	

		//W,b,R,B (checked 2015.08.26)
		//cuber.runAlgorithm("W,b,R,B");
		//cuber.runAlgorithm("b,r,B,w");
		
		
		//cuber.runAlgorithm("W,b,R,B,r,g,R,w,R,G,r,Y,Y,o,G,O");
		
		cube.runAlgorithm("R,g,o,B,W,y");
		cuber.getCube().printAsMap(System.out);
		cuber.getCube().save(System.out);		
	}
	
	
	@Test
	public void testSolve() throws Exception{
		
		FileInputStream input = new FileInputStream("etc\\cube07.xml");
		Cube theCube = Cube.load(input);
		theCube.printAsMap(System.out);
				
		DepthFirstSearch dfs = new DepthFirstSearch(theCube);

		dfs.setMaxDepth(10);
		String solution = dfs.solve();

		System.out.println(String.format("solution is: %s", solution));
		
	}
	
	public static void main(String args[]) throws Exception{
		
		new TestCuber().testSolve();
	}

}
