/*
* Copyright (C) 2015  wmmnpr@gmail.com
*
*This program is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*This program is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
package org.wmmnpr.ajcube;

/**
 * A person that solves a Cube.
 * @author wnpr
 *
 */
public class Cuber {
	
	private Cube cube;

	public Cube getCube() {
		return cube;
	}

	public void setCube(Cube cube) {
		this.cube = cube;
	}

	
	public void showCube(){
		this.cube.printAsMap(System.out);
	}
	
	public void solve(){
		
		//find most solved side.
		
		Cube solved = new Cube();
		

		
		
		
	}
}
