/*
* Copyright (C) 2015  wmmnpr@gmail.com
*
*This program is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*This program is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
package org.wmmnpr.ajcube;

/**
 * Not used. Attempt to use some AOP techniques during manipulation 
 * of the rubik's cube.
 * @author wnpr
 *
 */
public aspect CubeFaceRotator {
	
	
	pointcut rotateFaces(Cube c) : call(void Cube.rotateHideMe(Side, Direction)) && target(c); 
	
	void around(Cube c): rotateFaces(c) {
		
		final Side side = (Side)thisJoinPoint.getArgs()[0];
		final Direction dir = (Direction)thisJoinPoint.getArgs()[1];
		
		System.out.println(String.format("interceptor....before %s %s %s", c, side, dir));
		proceed(c);
		
		int iIdx=0; int jIdx=0; int kIdx=0;
		int iMax=3; int jMax=3; int kMax=3;
		
		System.out.println(String.format("interceptor....after %s %s %s", c, side, dir));	
		
		switch(side){
		
			case RED:				
				kIdx = 0;
				kMax = 1;
				break;
			case YELLOW:
				kIdx = 2;
				kMax = 3;				
				break;
			case WHITE:
				jIdx = 0;
				jMax = 1;
				break;
			case ORANGE:
				jIdx = 2;
				jMax = 3;				
				break;
			case BLUE:
				iIdx = 0;
				iMax = 1;
				break;
			case GREEN:
				System.out.println("interceptor GREEN");	
				iIdx = 2;
				iMax = 3;
				break;
			default:
				break;
				
		}
		
		System.out.println("interceptor before loop");	
		for(; kIdx < kMax; kIdx++){
			for(; jIdx < jMax; jIdx++){	
				for(; iIdx < iMax; iIdx++){						
					System.out.print(String.format("[%2s]:", ((9*kIdx)+(3*jIdx)+(1+iIdx))-1));
				}
			}
		}
		
		System.out.println("interceptor....exiting");	 
	}
	

}
