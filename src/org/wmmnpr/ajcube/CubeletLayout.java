/*
* Copyright (C) 2015  wmmnpr@gmail.com
*
*This program is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*This program is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
package org.wmmnpr.ajcube;

/**
 * Pattern for printing rubik's cube in 2d.
 * @author wnpr
 *
 */
public class CubeletLayout {

	
	//RY.WO.BG	
	public static final char SIDE_LAYOUT[][] = {
		
		// order of layout elements 
		// front to back    (red to orange)
		// top   to unten  (green to blue)
		// left  to right   (yellow to white)
		
		// front    top       left
		// back     unten     right
		//-------   ------    ------
		//F    B    T    U    L    R
		//R    O    G    B    Y    W
		{'R', ' ', 'G', ' ', 'Y', ' '}, //0
		{'R', ' ', 'G', ' ', ' ', ' '}, 
		{'R', ' ', 'G', ' ', ' ', 'W'},
		{'R', ' ', ' ', ' ', 'Y', ' '}, //3
		{'R', ' ', ' ', ' ', ' ', ' '},		
		{'R', ' ', ' ', ' ', ' ', 'W'},
		{'R', ' ', ' ', 'B', 'Y', ' '}, //6
		{'R', ' ', ' ', 'B', ' ', ' '},
		{'R', ' ', ' ', 'B', ' ', 'W'},
		
		{' ', ' ', 'G', ' ', 'Y', ' '}, //9
		{' ', ' ', 'G', ' ', ' ', ' '},
		{' ', ' ', 'G', ' ', ' ', 'W'},
		{' ', ' ', ' ', ' ', 'Y', ' '}, //12
		{' ', ' ', ' ', ' ', ' ', ' '},
		{' ', ' ', ' ', ' ', ' ', 'W'},
		{' ', ' ', ' ', 'B', 'Y', ' '}, //15	
		{' ', ' ', ' ', 'B', ' ', ' '},
		{' ', ' ', ' ', 'B', ' ', 'W'},
		
		{' ', 'O', 'G', ' ', 'Y', ' '}, //18
		{' ', 'O', 'G', ' ', ' ', ' '},
		{' ', 'O', 'G', ' ', ' ', 'W'},
		{' ', 'O', ' ', ' ', 'Y', ' '}, //21
		{' ', 'O', ' ', ' ', ' ', ' '},
		{' ', 'O', ' ', ' ', ' ', 'W'},
		{' ', 'O', ' ', 'B', 'Y', ' '}, //24
		{' ', 'O', ' ', 'B', ' ', ' '},	
		{' ', 'O', ' ', 'B', ' ', 'W'}		
				
	};
	
}
