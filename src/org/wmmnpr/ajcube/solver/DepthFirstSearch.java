/*
* Copyright (C) 2015  wmmnpr@gmail.com
*
*This program is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*This program is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
package org.wmmnpr.ajcube.solver;

/**
 * Depth first search.
 */
import java.util.ArrayList;

import org.wmmnpr.ajcube.Cube;
import org.wmmnpr.ajcube.Cubelet;
import org.wmmnpr.ajcube.Direction;
import org.wmmnpr.ajcube.Side;

public class DepthFirstSearch {
	
	private final static Cube GOAL = new Cube();
	

	private boolean isVerbose = true;
	
	private String PADDING = null;

	private Cube workCube;
	
	private Cube orgCube;
		
	private int maxDepth = 20;
	
	private int currMaxDepth = -1;
	
	private Cubelet edges[] = null;
	
	private final static Side sides[] = {Side.RED, Side.ORANGE, Side.GREEN, Side.BLUE, Side.YELLOW, Side.WHITE};
	private final static Direction dirs[] = {Direction.INVERTED, Direction.NORMAL};
	
	
	private ArrayList<String>solution = new ArrayList<String>();
	
	public DepthFirstSearch(Cube cube){
		this.workCube = new Cube(cube);
		this.orgCube = cube;
		
		int iedges[] = {1, 3, 5, 7, 9, 11, 15, 17, 19, 21, 23, 25};
		this.edges = new Cubelet[12];	
		int idx = 0;
		for(int edge : iedges){
			edges[idx++] = Cube.REF_CUBE.getCubelets()[edge];
		}
		
	}
	public String solve2(){
		
		if(isVerbose) createPadding();
	
		this.currMaxDepth = 6;
		if(solveRecursive(0)){
			ArrayList<String>reversed = new ArrayList<String>();
			for(int j = solution.size() - 1; j >= 0; j--){
				reversed.add(solution.get(j));
			}
			return reversed.toString();
		}
	
		return "";		
	}
	
	public String solve(){
	
		if(isVerbose) createPadding();
	
		int currDepth = 0;
		for(int i = 1; i < maxDepth; i++){
			this.currMaxDepth = i;			
			
			if(isVerbose) System.out.println(String.format("searching depth %s", this.currMaxDepth));
			
			if(solveRecursive(currDepth)){
				ArrayList<String>reversed = new ArrayList<String>();
				for(int j = solution.size() - 1; j >= 0; j--){
					reversed.add(solution.get(j));
				}
				return reversed.toString();
			}
		}
		return "";		
	}
	
	private boolean solveRecursive(int depth){
				
		if(this.workCube.equals(GOAL)) {
			System.out.println("*********Solution found*************");
			return true;
		}

		if(this.edgesCorrect()) {
			System.out.println("*********edges are correct*************");
			return true;
		}
		
				
		if(depth >= currMaxDepth){
			//System.out.println(" ");
			return false;
		}
		
		++depth;
			
		int sideIdx = depth % 6;
		Side side = null;
		for(int i=0; i < 6; i++){	
			side = sides[sideIdx];
			for(Direction dir : dirs){						
				workCube.rotate(side, dir);
				
				if(isVerbose) System.out.println(String.format("%s%s", PADDING.substring(0, depth-1), dir == Direction.INVERTED ? side.name().toLowerCase().substring(0, 1) : side.name().substring(0, 1)));
				
				if(solveRecursive(depth)){				
					solution.add(dir == Direction.INVERTED ? side.name().toLowerCase().substring(0, 1) : side.name().substring(0, 1));
					return true;
				}else{				
					workCube.rotate(side, dir == Direction.INVERTED ? Direction.NORMAL : Direction.INVERTED);
				}

			}
			
			sideIdx = (sideIdx + 1)% 6;
		}

		return false;
				
	}
	
	private void createPadding(){
		StringBuilder sb = new StringBuilder();
		for(int i=0; i <= maxDepth; i++){
			sb.append('.');
		}
		PADDING = sb.toString();
	}

	public int getMaxDepth() {
		return maxDepth;
	}

	public void setMaxDepth(int maxdepth) {
		this.maxDepth = maxdepth;
	}

	public boolean isVerbose() {
		return isVerbose;
	}

	public void setVerbose(boolean isVerbose) {
		this.isVerbose = isVerbose;
	}
	
	
	public boolean edgesCorrect(){
		return this.workCube.check(edges);
	}
	
}
