/*
* Copyright (C) 2015  wmmnpr@gmail.com
*
*This program is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*This program is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
package org.wmmnpr.ajcube;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Arrays;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

/**
 * A sub cube of a rubik's cube with 6 sides.
 * @author wnpr
 *
 */
@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
public class Cubelet {


	@XmlAttribute
	private int id = -1;
	
	/* 
	 * 
	 * A cubelete has 6 sides some of which are colored. The sides all face in one of 
	 * 6 directions. These directions are named according to the color of the center squares of the cube. For 
	 * instance, the red side can face in the red direction (R").
	 * index:      0    1    2    3    4    5
	 * name :      F    B    T    U    L    R  
	 * color:      R    O    G    B    Y    W
	 * start 
	 * direction:  R"   B"   G"   B"   Y"   W"
	 *
	 * U=Unten or bottom
	 */
	private char side[]; 
	
	public char[] getSides() {
		return side;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	/**
	 * JAXB constructor.
	 */
	public Cubelet(){
		
	}
	
	public Cubelet(int id){
		this.id = id;
		this.side = Arrays.copyOf(CubeletLayout.SIDE_LAYOUT[id], 6);
	}
	
	public Cubelet(int id, char side[]){
		this.id = id;
		this.side = Arrays.copyOf(side, 6);
	}
	
	
	public void rotate(final Side side, final Direction dir){
				
		//0    1    2    3    4    5
		//F    B    T    U    L    R
		//R    Y    W    O    B    G
		
		switch (side){
			case RED:
				if(dir.equals(Direction.NORMAL)){
					this.side[0] = this.side[0];
					this.side[1] = this.side[1];	
					
					char tmp = this.side[5];
					this.side[5] = this.side[2];
					this.side[2] = this.side[4];					
					this.side[4] = this.side[3];	
					this.side[3] = tmp;
				}else{
					this.side[0] = this.side[0];
					this.side[1] = this.side[1];	
					
					char tmp = this.side[4];
					this.side[4] = this.side[2];
					this.side[2] = this.side[5];	
					this.side[5] = this.side[3];
					this.side[3] = tmp;				
				}														
				break;				
			case YELLOW:
				if(dir.equals(Direction.INVERTED)){
					this.side[4] = this.side[4];
					this.side[5] = this.side[5];	
					
					char tmp = this.side[0];
					this.side[0] = this.side[3];
					this.side[3] = this.side[1];	
					this.side[1] = this.side[2];
					this.side[2] = tmp;		
				}else{
					this.side[4] = this.side[4];
					this.side[5] = this.side[5];	
					
					char tmp = this.side[0];
					this.side[0] = this.side[2];
					this.side[2] = this.side[1];	
					this.side[1] = this.side[3];
					this.side[3] = tmp;				
				}														
				break;				
			case WHITE:
				if(dir.equals(Direction.INVERTED)){
					char tmp = this.side[0];				
					this.side[0] = this.side[2];							
					this.side[2] = this.side[1];
					this.side[1] = this.side[3];				
					this.side[3] = tmp;
					
					this.side[4] = this.side[4];	
					this.side[5] = this.side[5];	
				}else{
					char tmp = this.side[0];				
					this.side[0] = this.side[3];							
					this.side[3] = this.side[1];
					this.side[1] = this.side[2];				
					this.side[2] = tmp;
					
					this.side[4] = this.side[4];	
					this.side[5] = this.side[5];						
				}					
				break;
				
			case ORANGE:				
				if(dir.equals(Direction.NORMAL)){
					char tmp = this.side[2];				
					this.side[2] = this.side[5];							
					this.side[5] = this.side[3];
					this.side[3] = this.side[4];				
					this.side[4] = tmp;
					
					this.side[0] = this.side[0];	
					this.side[1] = this.side[1];	
				}else{
					char tmp = this.side[2];				
					this.side[2] = this.side[4];							
					this.side[4] = this.side[3];
					this.side[3] = this.side[5];				
					this.side[5] = tmp;
					
					this.side[0] = this.side[0];	
					this.side[1] = this.side[1];					
				}					
				break;
				
			case BLUE:
				if(dir.equals(Direction.INVERTED)){
					char tmp = this.side[0];
					this.side[0] = this.side[5];
					this.side[5] = this.side[1];	
					this.side[1] = this.side[4];
					this.side[4] = tmp;
					
					this.side[2] = this.side[2];
					this.side[3] = this.side[3];							
				}else{
					char tmp = this.side[0];
					this.side[0] = this.side[4];
					this.side[4] = this.side[1];	
					this.side[1] = this.side[5];
					this.side[5] = tmp;
					
					this.side[2] = this.side[2];
					this.side[3] = this.side[3];				
				}					
				break;
				
			case GREEN:			
				if(dir.equals(Direction.NORMAL)){
					char tmp = this.side[0];
					this.side[0] = this.side[5];
					this.side[5] = this.side[1];				
					this.side[1] = this.side[4];	
					this.side[4] = tmp;
					this.side[2] = this.side[2];
					this.side[3] = this.side[3];							
				}else{
					char tmp = this.side[0];
					this.side[0] = this.side[4];
					this.side[4] = this.side[1];				
					this.side[1] = this.side[5];	
					this.side[5] = tmp;
					this.side[2] = this.side[2];
					this.side[3] = this.side[3];					
				}				
				break;
				 
			default:
				break;
		}		
	}
	
	@Override
	public boolean equals(Object arg){		
		if(arg instanceof Cubelet == false){
			return super.equals(arg);
		}else{
			Cubelet other = (Cubelet)arg;
			if(this.getId() == other.getId()){				
				for(int i=0; i<6; i++){
					if(!(this.side[i] == other.side[i])){
						return false;
					}
				}
				return true;
			}else{
				return false;
			}
		}		
	}
		
	/**
	 * 
	 * @param out
	 */
	public void print(PrintStream out){	
		for(int i=0; i<6; i++){
			out.print(String.format(":%s:", this.side[i]));
		}	
	}
	

}
