/*
* Copyright (C) 2015  wmmnpr@gmail.com
*
*This program is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*This program is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
package org.wmmnpr.ajcube;

import java.io.PrintStream;
import java.util.ArrayList;

/**
 * Store copies of Cubes.
 * @author wnpr
 *
 */
public class CubeTracker {
	
	
	public static class Move {
		
		public Side side;
		
		public Direction dir;
		
		public Move(final Side side, final Direction dir){
			this.side = side;
			this.dir = dir;
		}
		
	}
	
	private ArrayList<Move>moves = new ArrayList<Move>();
	
	public CubeTracker(){
		
	}
	
	public void add(final Side side, final Direction dir){
		this.moves.add(new Move(side, dir));
	}
	
	public void track(PrintStream out){
		
		ArrayList<Cube>copies = new ArrayList<Cube>();
		Cube cube = new Cube();
		for(Move move : moves){
			Cube copy = new Cube(cube);
			copies.add(copy);
			cube.rotate(move.side, move.dir);
		}
		
		copies.add(cube);
		
		for(int i=0; i<27; i++){		
			for(Cube cub : copies){			
				out.print(String.format("-%s-", cub.getCubelets()[i].getId()));				
			}		
		}
		
	}	
}
