/*
* Copyright (C) 2015  wmmnpr@gmail.com
*
*This program is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*This program is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
package org.wmmnpr.ajcube;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Calendar;
import java.util.Random;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * A standard 3x3 rubik's cube with 27 cubelets (sub cubes)
 * @author wnpr
 *
 */
@XmlRootElement(name="cube")
@XmlAccessorType(XmlAccessType.FIELD)
public class Cube  {
	
    @XmlElement
	private Cubelet cubelet[] = new Cubelet[27];
    
    
    public static final Cube REF_CUBE = new Cube();
	
    /**
     * Create solved cube.
     */
	public Cube(){
		for(int i=0; i<27; i++){
			this.cubelet[i]=new Cubelet(i);
		}
	}
	
	/**
	 * Create deap copy.
	 * @param copy
	 */
	public Cube(final Cube copy){		
		for(int i=0; i<27; i++){
			cubelet[i]=new Cubelet(copy.cubelet[i].getId(), copy.cubelet[i].getSides());		
		}				
	}
	
	/**
	 * rotate side of cube in specified direction.	
	 * @param side
	 * @param dir
	 */
	public void rotate(final Side side, final Direction dir) {
		//System.out.println(String.format("Cube.rotate %s %s", side, dir));					
		//rotate sides of rotated cubes
		int iBeg=0; int jBeg=0; int kBeg=0;
		int iMax=3; int jMax=3; int kMax=3;			
		int tIdx = 0; 
		boolean rowCol = true;
		switch(side){
		
			case RED:				
				kBeg = 0;
				kMax = 1;
				break;
			case YELLOW:
				iBeg = 0;
				iMax = 1;	
				break;
			case WHITE:
				iBeg = 2;
				iMax = 3;
				rowCol = false;
				break;
			case ORANGE:
				kBeg = 2;
				kMax = 3;
				rowCol = false;
				break;
			case BLUE:
				jBeg = 2;
				jMax = 3;
				break;
			case GREEN:	
				jBeg = 0;
				jMax = 1;
				rowCol = false;
				break;
			default:
				break;
				
		}
		
		//maxtrix with cubelets to move or rotate.
		int cubeSide [][] = {
				{0, 0, 0},
				{0, 0, 0},
				{0, 0, 0}
		}; 
	 			
		for(int k = kBeg; k < kMax; k++){
			for(int j = jBeg; j < jMax; j++){	
				for(int i = iBeg; i < iMax; i++){						
					//System.out.print(String.format("[%2s]:", ((9*k)+(3*j)+(1*i))));
					if(rowCol){
						cubeSide[tIdx/3][tIdx%3] = (9*k)+(3*j)+(1*i);
					}else{
						cubeSide[tIdx%3][tIdx/3] = (9*k)+(3*j)+(1*i);
					}
					
					tIdx++;
					this.cubelet[(9*k)+(3*j)+(1*i)].rotate(side, dir);
				}
			}
		}	
					
		moveCubeletes(cubeSide, dir);
		
	}
	/**
	 * move cubelets direction dir into proper cubicles.
	 * @param src
	 * @param dir
	 */
	private void moveCubeletes(int src[][], final Direction dir){
		
		Cubelet tmp = null;
			
		if(dir == Direction.NORMAL){
			//corners
			tmp = this.cubelet[src[0][0]];
			this.cubelet[src[0][0]] = this.cubelet[src[2][0]];
			this.cubelet[src[2][0]] = this.cubelet[src[2][2]];
			this.cubelet[src[2][2]] = this.cubelet[src[0][2]];		
			this.cubelet[src[0][2]] = tmp;
			
			//centers;
			tmp = this.cubelet[src[0][1]];
			this.cubelet[src[0][1]] = this.cubelet[src[1][0]];
			this.cubelet[src[1][0]] = this.cubelet[src[2][1]];
			this.cubelet[src[2][1]] = this.cubelet[src[1][2]];
			this.cubelet[src[1][2]] = tmp;		
		} else {
			//corners
			tmp = this.cubelet[src[0][0]];
			this.cubelet[src[0][0]] = this.cubelet[src[0][2]];
			this.cubelet[src[0][2]] = this.cubelet[src[2][2]];
			this.cubelet[src[2][2]] = this.cubelet[src[2][0]];		
			this.cubelet[src[2][0]] = tmp;
			
			//centers;
			tmp = this.cubelet[src[0][1]];
			this.cubelet[src[0][1]] = this.cubelet[src[1][2]];
			this.cubelet[src[1][2]] = this.cubelet[src[2][1]];
			this.cubelet[src[2][1]] = this.cubelet[src[1][0]];
			this.cubelet[src[1][0]] = tmp;				
			
		}
	
	}
	
	/**
	 * Print out cubelets according to position in array with orientation information.
	 * @param out
	 */
	public void print(PrintStream out){	
		//slots
		//for(int i = 0; i < 27; i++){			
		//	out.print(String.format("|%2s|", i));
		//}		
		out.println("\n--------------------------------------------------------------------------------------------------------");
		//cubelets
		for(int i = 0; i < 27; i++){			
			out.print(String.format("|%2s|%2s|",i, this.cubelet[i].getId()));
			this.cubelet[i].print(out);
			out.print("\n");
		}
	}
	
	/**
	 * compare to cubes including all cubelets and their orientations.
	 */
	@Override
	public boolean equals(final Object arg){
		
		if( arg instanceof Cube == false){
			return super.equals(arg);
		}
		
		Cube other = (Cube)arg;
		for(int i=0; i < 27; i++){			
			if(!this.cubelet[i].equals(other.cubelet[i])){
				return false;
			}
		}

		return true;
	}
	
	/**
	 * are cubelets of this cube the same as the provided ones
	 * @param cubelets
	 * @return
	 */
	public boolean check(Cubelet cubelets[]){
		
		for(Cubelet other : cubelets){
			if(this.cubelet[other.getId()].equals(other) == false){
				return false;
			}
		}
		
		return true;
		
	}
	
	/**
	 * Call deep copy
	 * @param toClone
	 * @return
	 */
	public Cube clone(final Cube toClone){
		return new Cube(this);
	}

	/**
	 * Get all cubelets composing cube.
	 * @return
	 */
	public Cubelet[] getCubelets() {
		return cubelet;
	}
	
	/**
	 * Print as map or cross according to PrintLayout.MAP_LAYOUT.
	 * @param out
	 */
	public void printAsMap(PrintStream out){
		
						  //24B, 25B, 26B
						  //21B, 22B, 23B
						  //18B, 19B, 20B
		
		//24L, 21L, 18L   //18T, 19T, 20T  //20R, 23R, 26R
		//15L, 12L,  9L   // 9T, 10T, 11T  //11R, 14R, 17R
		// 6L,  3L,  0L   // 0T,  1T,  2T  // 2R,  5R,  8R
		
						  //  0F,  1F,  2F  
		                  //  3F,  4F,  5F
		                  //  6F,  7F,  8F
		
						  //   6U,  7U,  8U
		                  //  15U, 16U, 17U
		                  //  24U, 25U, 26U
		
		
		String template = PrintLayout.MAP_LAYOUT.replaceAll("%", "%s");
		
		 out.println(String.format(template, 
				 //24B, 25B, 26B
				 //21B, 22B, 23B
				 //18B, 19B, 20B				 
				 this.cubelet[24].getSides()[1],
				 this.cubelet[25].getSides()[1],
				 this.cubelet[26].getSides()[1],
				 this.cubelet[21].getSides()[1],
				 this.cubelet[22].getSides()[1],
				 this.cubelet[23].getSides()[1],
				 this.cubelet[18].getSides()[1],
				 this.cubelet[19].getSides()[1],
				 this.cubelet[20].getSides()[1],
				 //24L, 21L, 18L   //18T, 19T, 20T  //20R, 23R, 26R				 
				 this.cubelet[24].getSides()[4],
				 this.cubelet[21].getSides()[4],
				 this.cubelet[18].getSides()[4],
				 this.cubelet[18].getSides()[2],
				 this.cubelet[19].getSides()[2],
				 this.cubelet[20].getSides()[2],
				 this.cubelet[20].getSides()[5],
				 this.cubelet[23].getSides()[5],
				 this.cubelet[26].getSides()[5],
				 //15L, 12L,  9L   // 9T, 10T, 11T  //11R, 14R, 17R
				 this.cubelet[15].getSides()[4],
				 this.cubelet[12].getSides()[4],
				 this.cubelet[9].getSides()[4],
				 this.cubelet[9].getSides()[2],
				 this.cubelet[10].getSides()[2],
				 this.cubelet[11].getSides()[2],
				 this.cubelet[11].getSides()[5],
				 this.cubelet[14].getSides()[5],
				 this.cubelet[17].getSides()[5],				 
				 // 6L,  3L,  0L   // 0T,  1T,  2T  // 2R,  5R,  8R				 
				 this.cubelet[6].getSides()[4],
				 this.cubelet[3].getSides()[4],
				 this.cubelet[0].getSides()[4],
				 this.cubelet[0].getSides()[2],
				 this.cubelet[1].getSides()[2],
				 this.cubelet[2].getSides()[2],
				 this.cubelet[2].getSides()[5],
				 this.cubelet[5].getSides()[5],
				 this.cubelet[8].getSides()[5],
				 //  0F,  1F,  2F  
                 //  3F,  4F,  5F
                 //  6F,  7F,  8F
				 this.cubelet[0].getSides()[0],
				 this.cubelet[1].getSides()[0],
				 this.cubelet[2].getSides()[0],
				 this.cubelet[3].getSides()[0],
				 this.cubelet[4].getSides()[0],
				 this.cubelet[5].getSides()[0],
				 this.cubelet[6].getSides()[0],
				 this.cubelet[7].getSides()[0],
				 this.cubelet[8].getSides()[0],
				 //   6U,  7U,  8U
                 //  15U, 16U, 17U
                 //  24U, 25U, 26U				 				 
				 this.cubelet[6].getSides()[3],
				 this.cubelet[7].getSides()[3],
				 this.cubelet[8].getSides()[3],
				 this.cubelet[15].getSides()[3],
				 this.cubelet[16].getSides()[3],
				 this.cubelet[17].getSides()[3],
				 this.cubelet[24].getSides()[3],
				 this.cubelet[25].getSides()[3],
				 this.cubelet[26].getSides()[3]
				
		));
		 
		 	
	}
	
	/**
	 * Randomize this cube.
	 * @param twists
	 * @param out
	 * @return
	 * @throws Exception
	 */
	public String randomize(int twists, OutputStream out)
		throws Exception {
		
		Cube theCube = new Cube();
		StringBuilder moves = new StringBuilder();
		
		Side sides[] = {Side.RED, Side.ORANGE, Side.GREEN, Side.BLUE, Side.YELLOW, Side.BLUE};
		Direction dirs[] = {Direction.INVERTED, Direction.NORMAL};
		
		Random ran = new Random();
		ran.setSeed(Calendar.getInstance().getTimeInMillis());
		
		Side side = null;
		Direction dir = null;
		for(int i=0; i<twists; i++){		
			side = sides[ran.nextInt(6)];
			dir = dirs[ran.nextInt(2)];			
			theCube.rotate(side, dir);	
			
			//add rotation to moves string
			String name = side.name();
			if(dir.equals(Direction.INVERTED)){
				name = name.toLowerCase();
			}
			
			if(moves.length() > 0){
				moves.append(',');
			}
			
			moves.append(name.charAt(0));
			
		}
				
		theCube.save(out);
		
		return moves.toString();
	
	}
	
	public void runAlgorithm(final String algorithm){
		
		Side side = null;
		Direction dir = null;
		String command = algorithm.replaceAll(",", "");
		for(int i=0; i < command.length(); i++){
			
			side = null;
			dir = null;
			
			switch(command.charAt(i)){
				case 'R':
					side = Side.RED;
					dir = Direction.NORMAL;
					break;
				case 'r' :
					side = Side.RED;
					dir = Direction.INVERTED;
					break;				
				case 'O':
					side = Side.ORANGE;
					dir = Direction.NORMAL;
					break;
				case 'o' :
					side = Side.ORANGE;
					dir = Direction.INVERTED;
					break;		
				//	
				case 'G':
					side = Side.GREEN;
					dir = Direction.NORMAL;
					break;					
				case 'g' :
					side = Side.GREEN;
					dir = Direction.INVERTED;
					break;		
				case 'B':
					side = Side.BLUE;
					dir = Direction.NORMAL;
					break;
				case 'b' :
					side = Side.BLUE;
					dir = Direction.INVERTED;
					break;						
				//	
				case 'Y':
					side = Side.YELLOW;
					dir = Direction.NORMAL;
					break;					
				case 'y' :
					side = Side.YELLOW;
					dir = Direction.INVERTED;
					break;		
				case 'W':
					side = Side.WHITE;
					dir = Direction.NORMAL;
					break;
				case 'w' :
					side = Side.WHITE;
					dir = Direction.INVERTED;
					break;						
			}
			
			if(side != null && dir != null){
				this.rotate(side, dir);
			}
			
		}
	}
	
	/**
	 * Persist cube to XML file. See etc/ for examples.
	 * @param out
	 * @throws Exception
	 */
	public void save(OutputStream out) throws Exception {	
		
	    JAXBContext jc = JAXBContext.newInstance(this.getClass());
	    Marshaller mar = jc.createMarshaller();
	    mar.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
	    mar.marshal(this, out);	   	
	    
	}
	
	/**
	 * Load Cube with content from XML file. See etc/ for examples.
	 * @param input
	 * @throws Exception
	 */
	
	public static Cube load(InputStream input) throws Exception {
			
		JAXBContext jc = JAXBContext.newInstance(Cube.class);		    
		Unmarshaller u = jc.createUnmarshaller();		    	
		Object obj = u.unmarshal(input);		
		return (Cube)obj;	    
		  
	}
	
}
