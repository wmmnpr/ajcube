# README #

I. Description:

AjCube is a rubik's cube programm, which allows the manipulation of the rubik's cube using standard cube operations. 
The rubik's cube is represented by the org.wmmnpr.ajcube.Cube java class. The class has a rotate method which requires a 
side, specified as a color (See org.wmmnpr.ajcube.Side) and a direction (See org.wmmnpr.ajcube.Direction). The colors 
are, going from front to back, top to bottom and left to right: red, orange, green, blue, yellow and green. See
the test classes in org.wmmnpr.ajcube.junit for examples of how to use the Cube class.
 
The cube has 27 cubicles, which are filled by 27 cubelets (sub cubes). The cubicles stay fixed in the cube and
the cubeletes move with each rotation. The cubicles and cubelets are numbered from 0 to 26 starting at the front (red) 
and moving left to right (yellow to white or i in the loops), top to bottom (green to blue or j in the loops) and front to back (red to orange or k in the loops) (See 
coordinate system show below). Note: the cubicles do not have class as the cubelets do. They can be imagined as being
the positions is the cubelete arrray of the Cube class. They only exist for visualization purposes.

See README.txt in project and source files for more information.